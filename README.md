# Yukaimaps Editor Layer Index

The goal of this project is to maintain a canonical representation of the layers available to Yukaimaps editors:

* [Yukaidi](https://gitlab.com/yukaimaps/yukaidi)

Both imagery and other raster data that is useful for mapping are within scope of the project.

This list is purely targeted at Yukaimaps and does not include layers only useful for other projects such as OpenStreetMap.

## Contributing

To add a new imagery source, add a new .geojson file to the in `sources` directory. See [CONTRIBUTING.md](CONTRIBUTING.md) for info about the attributes.

Install python requirements:

```shell
python -m pip install -U pip wheel PyYAML
python -m pip install -r requirements.txt
```

Delete old imagery files

```shell
make clean
```

Generate imagery files

```shell
make all
```

Additionals steps are required in Yukaidi editor:

```shell
npm run imagery ../editor-layer-index/imagery.json
git add data/imagery.json
git commit -m "update imagery"
```
